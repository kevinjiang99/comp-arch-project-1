/* Project 1 */
/* Kevin Jiang, Yuecen Wang */

.data

 .balign 4
input1:.asciz "Type your first string: " /*First prompt to type out  the first string */

.balign 4
input2: .asciz "Type your second string: " /*Second prompt to type out the second string */

.balign 4
outputmsg: .asciz "Concatenated String: " /* The prompt to type out the output concatenated String */ 
.balign 4
output: .asciz  "%c" /*printing output character datatype */

.balign 4
scan_pattern: .asciz "%c" /* scanning input characters */

.balign 4
return: .word 0  /* the original  value  of lr is stored in return */

.balign 4
string1: .skip 21  /* extra byte allocated for new line */

.balign 4
count1: .word 0 /* the original value of r6 is stored here */

.balign 4
count2: .word 0 /*the original  value of r5 is stored here */

.balign 4
countoffset: .word 0 /* the original value of r7 is stored here */

.balign 4
newline: .asciz "\n" /* the newline ASCII in order to print a newline at the end*/

.balign 4
errormsg: .asciz "Input must be 10 characters or less\n" /* An error message  if  a string has more than 10 characters*/
.text

.global main
main:
	ldr r1, return_addr  /* r1 <- &return */
	str lr, [r1]   /* *r1 <- lr, r1 points to return_addr right now, so you just stored the current address of lr into the memory slot of return_addr */

	ldr r3, count1_addr /* r3 <- &count1_addr */
	str r6, [r3] /* *r3 <- r6, r3 points to the count1_addr right now */
	mov r6, #0 /* the original value of the counting1 register r6 */

	ldr r3, count2_addr /* r3 <- &count2 */
	str r5, [r3] /* *r3 < - r5, r3 now points to the count2_addr */
	mov r5, #0 /* the original value of the counting2 register r5 */

	ldr r3, countoffset_addr /*r3 <- &countofset*/
	str r7, [r3] /* *r3 <- r7, r3 points to the countoffset_addr */
	mov r7, #10  /* the original value of r7,the count must be offset by 10 to find the limit of the concatenated string */

start:
	ldr r0, input1_addr    /* r0 <- input1_addr */
	bl printf /* call printf to print first prompt */

loopscan1:

	ldr r0, scan_pattern_addr  /* r0 <- &scan_pattern */
	ldr r1, string1_addr  /* r1 <- &number_read */
	add r1, r1, r6 /* offset the address stored inside of r1 by the count */
	bl scanf    /* call scanf to scan the first string */
	ldr r1, string1_addr /* r1 <- &string1_addr */
	ldrb r1, [r1, r6]	/* load character just stored back into r1 */

	cmp r1 ,#10 /*the comparison of the character stored in r1 and the \n charactore */
	beq next1
check_loopscan1:
	add r6, r6, #1 /* increment the index by 1 */
	cmp r6, #10 /* the comparison of the index with the limit of the length of 10 */
	bgt error1 /* go to error1 */

	b loopscan1 /* go back to loopscan1*/

next1:
	ldr r0, input2_addr /*r0 <- &input2 */
	bl printf /* call pintf to print the second prompt to scan the second string */

	add r7, r6, r7 /* r6, the count, can be used as index in the array, the offset it added to the account so the limit can be found*/
loopscan2:
	ldr r0, scan_pattern_addr /*same operations as loopscan1 to scan the second string */
	ldr r1, string1_addr
	add r1, r1, r6
	bl scanf
	ldr r1, string1_addr
	ldrb r1, [r1, r6]

	cmp r1, #10
	beq next2
check_loopscan2:
	add r6, r6, #1
	cmp r6, r7 /* Instea, the index is compared with the new limit */
	bgt error2 /* printing out the second error message if its out of limit */

	b loopscan2 /*go back to loopscan2 */

next2:
	ldr r0, outputmsg_addr /* r0 <- &outputmsg */
	bl printf /* call the printf function to  printout the output prompt */

loopprint:

	ldr r0, output_addr    /* r0 <-&output */
	ldr r1, string1_addr   /* r1 <- &string1 */
	ldrb  r1, [r1, r5]   /* loading the characters inside of the output string byte by byte */
	bl printf /* call the pringf function to printout the output string */
	add r5, r5, #1 /* r5 serves as the counting index of the ourput concatenated string */

	ldr r1, string1_addr /* r1 <- &string1 */
	ldrb r1, [r1, r5] /* loading the characters to print out the output string */
	cmp r1, #10 /* comparing the character stored in r1 with the \n character */
	beq success /* go to success */

	b loopprint /* go back from the start of looppint */

error1:
	ldr r0, errormsg_addr /* r0 <- &erromsg */
	bl printf /*call the printf function to print out the error message */

	mov r0, #21 /*to print out  the error code 21 */
	b end 

error2:
	ldr r0, errormsg_addr /* r0 <- errormsg */
	bl printf /*call the printf function to print out the error message */ 

	mov r0, #22/* print out the error code 22 */
	b end
success:
	ldr r0, newline_addr /* ro <- &newline */
	bl printf /* print out the new line */
 
	mov r0, r6 /* assign the value of r6 to r1 */
	b end
end:
	ldr r6, count1_addr /* load the addressing containing original value to r6 */
	ldr r6, [r6] /* r6 <- *r6 */
	ldr r5, count2_addr /* 5r <- &cout2 */
	ldr r5, [r5] /*r5 <- *r5 */
	ldr lr, return_addr   /* lr <- &return */
	ldr lr, [lr]  /* lr <- *lr */
	bx lr   /* return from main using restaored lr */

return_addr:  .word return
input1_addr:   .word input1
input2_addr: .word input2
output_addr: .word output
outputmsg_addr: .word outputmsg
scan_pattern_addr:  .word scan_pattern
string1_addr: .word string1
count1_addr: .word count1
count2_addr: .word count2
countoffset_addr: .word countoffset
newline_addr: .word newline
errormsg_addr: .word errormsg
/*External  */
.global printf
.global scanf
