/* Collatz Conjecture Assignment */
/* Kevin Jiang */

.text
.global main

main:
	mov r1, #123   /* r1 <- 123 */
	mov r0, #0      /* r0 counts how many iterations the program takes */
loop:
	cmp r1, #1     /* once r1 has the value 1, the program has reached its goal */
	beq end     /* branch to end if r1 holds the value 1 */
	add r0, r0, #1  /* increment the count */
	sub r2, r1, #1    
	lsr r3, r1,#1   /* compare the number in r1 divided by 2 wih the number in r1 minus 1 and then divided by 2 */
	lsr r4, r2,#1   /* if the two quotients are equal, then the number must be odd (since lsr rounds numbers down) */
	cmp r3, r4   
	beq case_equal   /* branch to case_equal if the two quotients are equal */
case_different:   /* Case when the number in r1 is even */
	lsr r1, r1,#1   /* divide number in r1 by 2 */
	b loop   /* branch to loop */
case_equal:  	/* Case when the number in r1 is odd */
	lsl r3, r1,#1	/* multiply number in r1 by 2, store in r3 */
	add r3, r3, r1	 /* add the number in r3 with the number in r1, store in r3 */

	add r1, r3, #1   /* add 1 to 3 times the number in r1 */
	b loop    /* branch to loop */
end:
	bx lr
