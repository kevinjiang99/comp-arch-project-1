/* Fibonacci Sequence Homework */
/* Kevin Jiang */

.text

.global main

main:
	mov r1, #1  /* first number of fib sequence */
	mov r2, #1   /* second number of fib sequence */
	mov r4, #2   /* index of the fib sequence */
loop:
	add r4, r4, #1  /* increment index by 1 */
	mov r3, r2    /* r3 <- r2 */
	add r2, r2, r1   /* r2 <- r2 + r1, so now r2 contains the next fib sequence number */
	mov r1, r3   /* r1 <- r3 (moves the value of r2 at the start of iteration into r1)  */
	cmp r4, #10  /* check to see if we reached the 10th fib sequence number */
	beq end   /* if r4 is equal to 10, branch to end */
	b loop  /* branch to loop */
end: 
	mov r0, r2 /* r0 <- r2 so that error code will be the 10th fib sequence number */ 
	bx lr

