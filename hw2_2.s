.global main

main:
	mov r1, #1
	mov r2, #1 /* the first two fibonacci number is 1*/
	mov r3, #2 /* the nth fibonacci number*/

loop:
	add r3,r3, #1 /* keep tracking the nth fibonacci number*/
	mov r4,r2 /* move the (n-1)th to  the next register*/
	add r2,r2,r1
	mov r1, r4 /*move the next fibonacci number to the first register*/
	cmp r3,#10
	beq end /* if its the 10th number, exit*/
	b loop
end:
	mov r0, r2
	bx lr

