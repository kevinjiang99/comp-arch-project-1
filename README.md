# Comp Arch Project 1
Move the Makefile and the source file project1.s to the directory

Type make

Type ./project1

You will see "type the first string"

Type your first string

You will see "type the second string"

Type your second string

The concatenated string will be printed

If the strings you typed are above the limit (more than 10 characters), you will receive an error message
