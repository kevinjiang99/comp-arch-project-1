.global main

main:
	mov r0, #0 /*register for the number of iteration*/
	mov r1, #123 

loop:
	cmp r1, #1
	beq end /* Compare the number with 1, if it ends at 1, the loop will end*/
	add r0, r0, #1 /*Keep tracking on the number of iteration*/
	sub r2, r1, #1
	lsr r3, r1, #1
	lsr r4, r2, #1 /*left shift divides the number by 2*/
/*when comparing the number-1 and the number itself, if the values are equal, that means the number is odd*/
	cmp r3, r4
	beq equal
different:
	lsr r1, r1, #1 /* if the number is even, divide the number by 2*/
	b loop
equal:
	mov r4, r1
	add r3, r1, r4
	add r3, r3 ,r1 /* add r1 three times*/
	add r1, r3, #1
 	b loop
end:
	bx lr
